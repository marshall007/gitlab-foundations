export enum ButtonVariant {
  default = 'default',
  confirm = 'confirm',
  success = 'success',
  warning = 'warning',
  danger  = 'danger',
  info    = 'info',
};
