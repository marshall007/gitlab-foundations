import { defineCustomElement } from 'vue';
import Button from './components/Button.ce.vue';
import Icon from './components/Icon.ce.vue';

export const GitLabButton = defineCustomElement(Button);
export const GitLabIcon = defineCustomElement(Icon);

export function register(prefix: string = 'gitlab') {
  customElements.define(`${prefix}-button`, GitLabButton);
  customElements.define(`${prefix}-icon`, GitLabIcon);
}
