import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import customElementsManifest from 'vite-plugin-cem';

// https://vitejs.dev/config/
export default defineConfig({
  base: '',
  build: {
    sourcemap: true,
    outDir: 'dist',
  },
  plugins: [
    vue({customElement: true}),
    customElementsManifest({
      files: [
        './src/main.ts'
      ]
    }),
  ],
})
